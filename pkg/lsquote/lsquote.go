package lsquote

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Fetches the price for the given symbol from the LANG & SCHWARZ webpage.
// The symbol can be the `WKN` number or their human readable name.
// The Tesla price can be retrieved with the following symbols:
// * `A1CX3T`
// * `tesla-motors-aktie`
func Value(symbol string) (float64, error) {
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	url := "https://www.ls-tc.de/en/stock/" + symbol

	resp, err := client.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, errors.New("Wrong status code from stock exchange")
	}

	html, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	// <span source="lightstreamer" table="quotes" item="41933@1" field="mid" decimals="4">976.6500</span> €
	re := regexp.MustCompile(`field="mid" decimals="4">([\d\.\,]+)<`)
	matches := re.FindStringSubmatch(string(html))
	if len(matches) != 2 {
		return 0, fmt.Errorf("Could not find value for stock. No. matches = %v", len(matches))
	}

	return valueStrToFloat(matches[1])
}

func valueStrToFloat(valueStr string) (float64, error) {
	v := strings.ReplaceAll(valueStr, `,`, ``)
	return strconv.ParseFloat(v, 64)
}
