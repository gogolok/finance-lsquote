package lsquote

import (
	"testing"
)

func TestValueStrToFloatConvert(t *testing.T) {
	var v float64
	var err error

	v, err = valueStrToFloat("1.0000")
	if err != nil {
		t.Fatal("Expected 1.0000, got err: ", err)
	}
	if v != 1.0000 {
		t.Fatal("Expected 1.0000, got ", v)
	}

	v, err = valueStrToFloat("1,2345.0000")
	if err != nil {
		t.Fatal("Expected 12345.0000, got err ", err)
	}
	if v != 12345.0000 {
		t.Fatal("Expected 12345.0000, got ", v)
	}

	v, err = valueStrToFloat("1,b345.a000")
	if err == nil {
		t.Fatal("Expected parsing of 1,b345.a000 tof fail")
	}
}
