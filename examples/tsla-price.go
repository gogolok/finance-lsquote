package main

import (
	"fmt"

	"gitlab.com/gogolok/finance-lsquote/pkg/lsquote"
)

func main() {
	tslaWkn := `A1CX3T`
	v, err := lsquote.Value(tslaWkn)
	fmt.Printf("TSLA: %v ; err = %v\n", v, err)
}
