package cmd

import (
	"fmt"

	"gitlab.com/gogolok/finance-lsquote/pkg/lsquote"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(priceCmd)
}

var priceCmd = &cobra.Command{
	Use:  "price [wkn-or-lsname]",
	Args: cobra.ExactArgs(1),
	RunE: price,
}

func price(cmd *cobra.Command, args []string) error {

	v, err := lsquote.Value(args[0])
	if err != nil {
		return err
	}

	fmt.Printf("%v\n", v)

	return nil
}
