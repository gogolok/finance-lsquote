package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "finance-lsquote",
	Short: "Return price from Lang & Schwarz exchange",
	Long:  `Fetch the current price from Lang & Schwarz website for private usage.`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
