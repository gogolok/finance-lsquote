# finance-ls-quote

Get prices from the stock exchange Lang & Schwarz via their public website.
For **personal** use only.

## Command Line Tool

```
$ finance-lsquote price  tesla-motors-aktie
953.9

# WKN
$ finance-lsquote price A1CX3T
953.9

# ISIN
$ finance-lsquote price US88160R1014
953.9
```

## Package

```go
tslaWkn := `A1CX3T`
v, err := lsquote.Value(tslaWkn)
fmt.Printf("TSLA: %v ; err = %v\n", v, err)
```

### Demo

```shell
$ go run examples/tsla-price.go
TSLA: 992.5 ; err = <nil>
```
