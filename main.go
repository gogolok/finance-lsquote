package main

import (
	"gitlab.com/gogolok/finance-lsquote/cmd"
)

func main() {
	cmd.Execute()
}
